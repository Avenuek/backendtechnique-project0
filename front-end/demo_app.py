"""
This module is th heart of this system
"""
import logging
import tornado.ioloop as ioloop
import tornado.httpserver as httpserver
import tornado.web
from handler.control_center_handler import ControlCenterHandler
from handler.displayroom_handler import DisplayRoomHandler
from handler.sos_caller_handler import SOSCallerHandler

LOG = logging
LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

if __name__ == "__main__":
    LOG.info("starting stupid frontend at port 8081... ")
    HTTP_SERVER = httpserver.HTTPServer(tornado.web.Application([
        (r"/[^.].[a-zA-Z0-9_-]+[^/.]/display$", DisplayRoomHandler),
        (r"/[^.].[a-zA-Z0-9_-]+[^/.]/control$", ControlCenterHandler),
        (r"/ [ ^ .].[a-zA-Z0-9_-]+/[^.][.a-zA-Z0-9_-]+[^/][a-zA-Z0-9_-]+$", SOSCallerHandler)
        ], debug=True))
    HTTP_SERVER.listen(8081)
    ioloop.IOLoop.current().start()

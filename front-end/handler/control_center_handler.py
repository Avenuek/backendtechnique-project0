import os
import tornado.web
import requests

class ControlCenterHandler(tornado.web.RequestHandler):
    def initialize(self):
        path = self.request.path
        path = path.split("/")
        path.remove('')
        self.bucket_name = path[0]

    def get(self):
        resp = requests.get(f"http://localhost:8080/{self.bucket_name}?list")
        objects = (resp.json()['objects'])
        # print("\n", objects, "\n")
        html = "./../html/control_center.html"
        list_example = []
        for obj in objects:
            file = obj['name']
            extensions = set(["mov", "mp4", "m4a", "3gp", "3g2", "mj2"])
            print("\n", file.split(".")[-1] in extensions, "\n")
            if file.split(".")[-1] in extensions :
                list_example.append(file)
        print("\n", list_example, "\n")
        self.render(html, 
            title=f"{self.bucket_name}", 
            bucketname=self.bucket_name,
            list_example=list_example
            )

#!/usr/bin/env python3
import os
import logging
import json
import uuid
import redis
import hashlib
import requests
import tornado.web

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
QUEUE_NAME = 'queue:thumbnail'
DOAMIN = os.getenv("SOS_DOMAIN", "localhost")
PORT = os.getenv("SOS_PORT", 8080)
BASE_URL = f"http://{DOAMIN}:{PORT}"

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = packed_task
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)


def get_content(path, start=None, end=None):
            """Retrieve the content of the requested resource which is located
            at the given absolute path.

            This method should either return a byte string or an iterator
            of byte strings.  The latter is preferred for large files
            as it helps reduce memory fragmentation.
            """
            with open(path, "rb") as file:
                if start is not None:
                    file.seek(start)
                if end is not None:
                    remaining = end - (start or 0)
                else:
                    remaining = None
                while True:
                    chunk_size = 64 * 1024
                    if remaining is not None and remaining < chunk_size:
                        chunk_size = remaining
                    chunk = file.read(chunk_size)
                    if chunk:
                        if remaining is not None:
                            remaining -= len(chunk)
                        yield chunk
                    else:
                        if remaining is not None:
                            assert remaining == 0
                        return

def execute_video_to_gift(log, task):
    # send request to sos asking for video
    task = tornado.escape.json_decode(task)
    file = task.get("file")
    bucket = task.get("bucket")
    text = task.get("text")
    #download video to this worker
    LOG.info(f"starting download file:{file} from bucket:{bucket} in sos")
    resp = requests.get(f"{BASE_URL}/{bucket}/{file}",headers={"Range":"Bytes=0-"})
    path_video = f"./{file}"
    with open(path_video,"wb") as fo:
        for each_chunk in resp.iter_content(chunk_size=256):
            fo.write(each_chunk)
    fo.close()
    LOG.info("done downloading")
    # make gif
    LOG.info(f"making gif")
    # DEFAULT_MSG = "DEFAULT TEXT"
    gif_file = f"{file}.gif"
    # video_to_gif(path_video, gif_file, text)
    print("\n", f"./makegif {path_video} {gif_file} \"{text}\" ", "\n")
    
    os.system(f"./makegif {path_video} {gif_file} \"{text}\" ")
    LOG.info("done gif")
    #upload back to sos
    ## create tickket
    gif_path = f"./{gif_file}"
    upload_ticket = requests.post(f"{BASE_URL}/{bucket}/{gif_file}?create")
    if upload_ticket.status_code != 200:
        #gif already exist
        os.remove(path_video)
        os.remove(gif_path)
        return
    ## upload .gif back
    m = hashlib.md5()
    gif_bytes_content = get_content(f"./{file}.gif")
    mydata =b''
    for data_chunk in gif_bytes_content:
            mydata+=data_chunk
            m.update(data_chunk)
    md5 = m.hexdigest()
    upload_resp = requests.put(f"{BASE_URL}/{bucket}/{gif_file}?partNumber=1", data=mydata, headers={"Content-MD5": f"{md5}"})
    if upload_resp.status_code != 200:
        #something wrong at upload
        os.remove(path_video)
        os.remove(gif_path)
        return
    complete_resp = requests.post(f"{BASE_URL}/{bucket}/{gif_file}?complete")
    # os.remove(path_video)
    # os.remove(gif_path)

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info(
        'Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn,
        QUEUE_NAME,
        lambda task_descr: execute_video_to_gift(named_logging, task_descr))

if __name__ == '__main__':
    main()

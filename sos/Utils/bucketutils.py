import time
import pymongo
import os
import sys
import shutil
OPERATIONFAILURE = pymongo.errors.OperationFailure
class BucketUtills:
    def __init__(self):
        return
    def create_bucket(self, bucket_name, db):
        try:
            t = int(time.time())
            db.buckets.insert_one(
                {'created': t, "modified": t, 'name': bucket_name,'_id': bucket_name})
            path = f"./buckets/{bucket_name}"
            if not os.path.exists(path):
                os.makedirs(path)
            return {'created': t, "modified": t, 'name': bucket_name}
        except pymongo.errors.DuplicateKeyError:
            return "already exist"

    def delete_bucket(self, bucket_name, db):
        try:
            result = db.buckets.delete_one({'_id': bucket_name})
            if (result.raw_result['n'] == 0):
                return "cannot delete"
            db[bucket_name].drop()
            path = f"./buckets/{bucket_name}"
            if os.path.exists(path):
                shutil.rmtree(path)
            return result
        except OPERATIONFAILURE:
            return "cannot delete"
    
    def list_bucket(self, bucket_name, db): 
        try:
            result = db.buckets.find_one({"_id": bucket_name})
            if result == None:
                return "bucket does not exist."
            objects_list = []
            objs = db[bucket_name].find()
            for o in objs:
                objects_list.append(
                    dict({k: o[k]for k in o.keys() if k != "_id" and k != 'complete' and k != 'parts_data'})
                    )
            ret_json = dict({k:result[k] for k in result.keys() if k != "_id"})
            ret_json["objects"] = objects_list
            return ret_json
        except OPERATIONFAILURE:
            return "bucket does not exist."

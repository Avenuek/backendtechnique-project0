import time
import hashlib
import pymongo
import os
import tornado
from Utils.fileutils import FileUtils
OPERATIONFAILURE = pymongo.errors.OperationFailure

class ObjectUtills:
    def __init__(self):
        self.FileUtils = FileUtils()
        return
    def creat_upload_ticket(self, bucket_name, object_name, db):
        result = db.buckets.find_one({'_id': bucket_name})
        if result == None:
            return "bucket does not exist"
        try:
            t=int(time.time())
            db[bucket_name].insert_one({"modified": t, 
                                        "created": t, 
                                        'name': object_name, 
                                        '_id': object_name, 
                                        'complete': False, 
                                        'parts_data': dict()})
        except OPERATIONFAILURE:
            return "duplicate object name or ticket already exist"
        return "ticket: Done something with it.."
    def bucket_update_modified_time(self,bucket_name, db, time):
        """ update midified time for bucket"""
        t = time
        bucket = db.buckets.find_one({'_id': bucket_name})
        bucket['modified'] = t
        db.buckets.save(bucket)
        return
    def object_update_modified_time(self, bucket_name, object_name, db, time):
        """ update midified time for object"""
        t = time
        obj = db[bucket_name].find_one({'_id': object_name})
        obj['modified'] = t
        db[bucket_name].save(obj)
        return

    def delete_object(self, bucket_name, object_name, db):
        result = db[bucket_name].find_one({'_id': object_name})
        if result == None:
            return "Object does not exist"
        db[bucket_name].delete_one({'_id': object_name})
        self.bucket_update_modified_time(bucket_name, db, int(time.time()))
        return object_name+" has been deleted"

    def delete_part(self, bucket_name, object_name, db,partNumber):
        obj = db[bucket_name].find_one({'_id': object_name})
        if obj == None:
            return "Object does not exist"
        if obj['complete'] :
            return "cannot delete part becasue the upload already completed"
        file_name = object_name + "_part_" + str(format(partNumber, "05"))
        if file_name in obj['parts_data'].keys():
            obj['parts_data'].pop(file_name)
            obj['modified'] = int(time.time())
            db[bucket_name].save(obj)
            return file_name +": this part has been sucessfully deleted", 200
        else:
            return file_name + ": this part does not exist or maybe deleted before", 400
        
    def complete_upload(self, bucket_name, object_name, db):
        error=[]
        result = db.buckets.find_one({'_id': bucket_name})
        if result != None:
            BUCKET_OK = True
        else:
            BUCKET_OK = False
            error.append("InvalidBucket")
        #check if Object does exist
        result = db[bucket_name].find_one({'_id': object_name})
        if result != None:
            OBJECT_OK = True
        else:
            OBJECT_OK = False
            error.append("InvalidObjectName")
            error.append("ObjectDoesNotExist")
        #check if there is a ticket
        if OBJECT_OK and "complete" in result.keys():
            TICKET_OK = True
        else:
            TICKET_OK = False
            error.append("NoUploadTicket")
        #check if not being complete before    
        if OBJECT_OK and not result['complete']:
            NOT_COMPLETED = True
        else:
            NOT_COMPLETED = False
            if TICKET_OK:
                error.append("ObjectAlreadyCompleted")
        if BUCKET_OK and TICKET_OK and NOT_COMPLETED and OBJECT_OK:
            #update db status
            parts = 0
            md5 = b""
            length = 0
            obj = db[bucket_name].find_one({"_id": object_name})
            files = obj['parts_data'].keys()
            sorted(files)
            obj['complete'] = True
            for each_file in files:
                md5 += obj['parts_data'][each_file][0]
                length += int(obj['parts_data'][each_file][1])
                parts += 1
            #store etag
            m=hashlib.md5()
            m.update(md5)
            md5 = m.hexdigest()
            if length == 0:
                etag = ""
            else:
                etag = md5 + "-" + str(parts)
            t = int(time.time())
            obj['eTag'] = etag
            obj['length'] = length
            obj['modified'] = t
            db[bucket_name].save(obj)
            bucket = db.buckets.find_one({"_id":bucket_name})
            bucket['modified'] = t
            db.buckets.save(bucket)
            return {"eTag": etag,"length": length,"name":object_name}, 200
        else:
            error_str = error[0]
            for i in range(1, len(error)):
                error_str += "|"
                error_str += error[i]
            return {"eTag":"","length":0,"name":object_name, "error": error_str},400

    def upload_by_part(self,bucket_name, object_name, db, 
                            md5_url, content_len_url,partNumber,body_text):
        #check md5
        m = hashlib.md5()
        m.update(body_text)
        error = []
        if (m.hexdigest() == md5_url):
            MD5_OK = True
        else:
            MD5_OK = False
            error.append("MD5Mismatched")
        # check Contene-length
        if len(body_text) == content_len_url: 
            CONTENT_LENGTH_OK = True
        else:
            CONTENT_LENGTH_OK = False
            error.append("LengthMismatched")
        # check Part number is in range(10000)
        if (1 <= partNumber and partNumber <= 10000): 
            PART_NUMBER_OK = True
        else:
            PART_NUMBER_OK = False
            error.append("InvalidPartNumber")
        #check if Bucket does exist
        result = db.buckets.find_one({'_id': bucket_name})
        if result != None:
            BUCKET_OK = True
        else:
            BUCKET_OK = False
            error.append("InvalidBucket")
        #check if Object does exist
        result = db[bucket_name].find_one({'_id': object_name}) 
        if result != None :
            OBJECT_OK = True
        else :
            OBJECT_OK = False
            error.append("InvalidObjectName")
        #check if ticket is ok
        if OBJECT_OK and "complete" in result.keys():
            TICKET_OK = True
        else:
            TICKET_OK = False
            error.append("NoUploadTicket")
        if OBJECT_OK and not result['complete']:
            NOT_COMPLETED = True
        else:
            NOT_COMPLETED = False
            if TICKET_OK:
                error.append("ObjectAlreadyCompleted")

        EVERYTHING_OK = (MD5_OK
                        and CONTENT_LENGTH_OK 
                        and PART_NUMBER_OK 
                        and BUCKET_OK
                        and TICKET_OK
                        and OBJECT_OK
                        and NOT_COMPLETED)

        if EVERYTHING_OK:
            #todo: write recieved file to local storage
            file_name = object_name + "_part_" + str(format(partNumber, "05"))
            path = "./buckets/"+bucket_name+"/"+file_name
            with open(path, "wb") as fo:
                fo.write(body_text)
            fo.close()
            obj = db[bucket_name].find_one({"_id": object_name})
            if not obj['complete']:
                t = int(time.time())
                obj["modified"] = t
                obj['parts_data'][file_name] = [m.digest(), content_len_url]
                db[bucket_name].save(obj)  # save back to db
                code = 200
                return {"md5": md5_url, "length": content_len_url, "partNumber": partNumber},code
            else:
                OBJECT_ALREADY_COMPLETE = True
        if (not EVERYTHING_OK) or (OBJECT_ALREADY_COMPLETE) :
            error_str = error[0]
            for i in range(1,len(error)):
                error_str+="|"
                error_str +=error[i]
            code = 400
            return {"md5": md5_url, "length": content_len_url, "partNumber": partNumber,"error":error_str},code
    
    def prepare_download(self, bucket_name, object_name, db, content_range):
        """ """
        obj = db[bucket_name].find_one({"_id": object_name})
        if obj == None: #object exist
            return "Object: "+ object_name+" not found in Bucket: "+bucket_name, 404
        if "complete" not in obj.keys(): #upload completed
            return "Uploading is not completed", 400
        #prepare range
        start = int(content_range[0])
        end = content_range[1]
        length = int(obj['length'])
        if end == '':
            end = int(obj['length'])
        end = int(end)
        if start > end or start > length:
            return "invalid range", 400
        # do upload checking
        file_parts = list(obj['parts_data'].keys())
        sorted(file_parts)
        range_lst = [int(obj['parts_data'][f][1]) for f in file_parts]
        start_file_index, start_byte = self.FileUtils.seek_part(start, range_lst)
        end_file_index, end_byte = self.FileUtils.seek_part(end, range_lst)
        return file_parts, (start_file_index, start_byte), (end_file_index, end_byte)

    def get_download_chuck(self, bucket_name, prep_download_package):
        file_list, tuple_start, tuple_end = prep_download_package
        start_file_index, start_byte = tuple_start
        end_file_index, end_byte = tuple_end
        file_range = range(start_file_index, end_file_index + 1)

        SINGLE_FILE = (len(file_range) == 1)
        for f_i in file_range:
            file = file_list[f_i]
            path = "./buckets/" + bucket_name + "/" + file
            if SINGLE_FILE:
                # SINGLE_FILE = False
                data_buffer = self.FileUtils.get_content(path, start_byte, end_byte)
                yield data_buffer
            else:  # mulitiple files
                if f_i is start_file_index:
                    data_buffer = self.FileUtils.get_content(path, start=start_byte)
                elif f_i is end_file_index:
                    data_buffer = self.FileUtils.get_content(path, end=end_byte)
                else:
                    data_buffer = self.FileUtils.get_content(path)
                yield data_buffer

    def add_metadata(self, bucket_name, object_name, db,new_metadata):
        """ this function add metadata to database"""
        key, val = new_metadata
        obj = db[bucket_name].find_one({"_id": object_name})
        if "metadata" not in obj.keys():
            #create new one
            obj["metadata"] = dict()
        obj["metadata"][key.decode('utf8')] = val.decode('utf8')
        db[bucket_name].save(obj)
        t = int(time.time())
        self.bucket_update_modified_time(bucket_name, db, t)
        self.object_update_modified_time(bucket_name, object_name, db, t)
        return "sucessfully added metadata", 200
    
    def delete_metadata(self, bucket_name, object_name, db, new_metadata):
        """ this function add metadata to database"""
        key, val = new_metadata
        key = key.decode('utf8')
        obj = db[bucket_name].find_one({"_id": object_name})
        if "metadata" not in obj.keys():
            #create new one
            obj["metadata"] = dict()
        if key not in obj['metadata'].keys():
            return "couldnot delete since key: "+key+" does not exist",404
        del obj['metadata'][key]
        db[bucket_name].save(obj)
         t = int(time.time())
        self.bucket_update_modified_time(bucket_name, db, t)
        self.object_update_modified_time(bucket_name, object_name, db, t)
        return "sucessfully deleted metadata", 200

    def get_metadata(self, bucket_name, object_name, db, new_metadata):
        """ this function find and return a metadata in object if the key is given,
            the function will seek the metadata conrespnd to the key otherwise return all of metadata 
        """
        key = new_metadata[0]
        obj = db[bucket_name].find_one({"_id": object_name})
        if "metadata" not in obj.keys():
            #create new one
            obj["metadata"] = dict()
        if key != None:
            # print("\n",obj['metadata'],"\n")
            ret_json = {key.decode('utf8'):obj['metadata'][key.decode('utf8')]}
            return ret_json, 200
        ret = {}
        for k in obj['metadata'].keys():
                ret[k] = obj['metadata'][k]
        return obj['metadata'],200
    
    def delete_all_gif(self, bucket_name,db):
        """ 
        this function is for deleting all gif in sos
        """
        bucket = db[f"{bucket_name}"]
        if bucket == None:
            return 'bucket does not exist', 400
        for obj in bucket:
            extension = obj["name"].split(".")[-1].lower()
            if extension == "gif":
                result=self.delete_object(self, bucket_name, obj['name'], db)
        t = int(time.time())
        self.bucket_update_modified_time(bucket_name, db, t)
        self.object_update_modified_time(bucket_name, object_name, db, t)
        return "all gifs are deleted", 200
        # bucket.


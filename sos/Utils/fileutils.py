class FileUtils:
    def __init__(self):
        return
    def seek_part(self, targetByte, listFile):
        for file_number in range(len(listFile)): #start at 0
            if targetByte - listFile[file_number] > 0:
                targetByte -= listFile[file_number]
            else:
                if file_number == 0:
                    return file_number, targetByte
                else:
                    return file_number, targetByte - 1

    def get_content(self, path, start=None, end=None):
            """Retrieve the content of the requested resource which is located
            at the given absolute path.
            This method should either return a byte string or an iterator
            of byte strings.  The latter is preferred for large files
            as it helps reduce memory fragmentation.
            """
            with open(path, "rb") as file:
                if start is not None:
                    file.seek(start)
                if end is not None:
                    remaining = end - (start or 0)
                else:
                    remaining = None
                while True:
                    chunk_size = 64 * 1024
                    if remaining is not None and remaining < chunk_size:
                        chunk_size = remaining
                    chunk = file.read(chunk_size)
                    if chunk:
                        if remaining is not None:
                            remaining -= len(chunk)
                        yield chunk
                    else:
                        if remaining is not None:
                            assert remaining == 0
                        return

""" Everything Buckets """
import logging
import tornado
from Utils.bucketutils import BucketUtills
class BucketHandeler(tornado.web.RequestHandler):
    def initialize(self, db):
        path = self.request.path
        path = path.split("/")
        path.remove('')
        self.bucket_name = path[0]
        self.db = db
        self.BucketUtills = BucketUtills()

    def does_bucket_exist(self):
        result = self.db.buckets.find_one({"_id": self.bucket_name})
        return result != None

    def post(self,*args, **kwargs):
        args = dict(self.request.arguments).keys()
        args_num = len(args)
        if 'create' in args and args_num == 1:
            #create bucket
            bucket_name = self.bucket_name
            response= self.BucketUtills.create_bucket(bucket_name, self.db)
            if response != "already exist":
                self.set_status(200)
                self.set_header("Content-Type", 'application/json')
                self.write(tornado.escape.json_encode(response))
            else:
                self.write("duplicate db name")
                self.set_status(400)
        else:
            self.write("not found")
            self.set_status(404)
        self.finish()

    def delete(self, *args, **kwargs):
        args = dict(self.request.arguments).keys()
        args_num = len(args)
        if not self.does_bucket_exist():
            self.write("Bucket not found")
            self.set_status(404)
            self.finish()
        if 'delete' in args and args_num == 1:
            #delete bucket
            bucket_name = self.bucket_name
            response = self.BucketUtills.delete_bucket(bucket_name,self.db)
            if response != "cannot delete":
                self.write(bucket_name+" has been sucessfuly deleted")
                self.set_status(200)
            else:
                self.write("cannot delete")
                self.set_status(400)
        else:
            self.write("not found")
            self.set_status(404)
        self.finish()

    def get(self, *args, **kwargs):
        #list bucket
        args = dict(self.request.arguments).keys()
        args_num = len(args)
        if 'exist' in args and args_num == 1:
            # print("\n",type(self.bucket_name),"\n")
            if self.does_bucket_exist():
                ret_json = {"found": True}
                self.set_status(200)
            else:
                ret_json = {"found": False}
                self.set_status(404)
            self.set_header("Content-Type", 'application/json')
            self.write(tornado.escape.json_encode(ret_json))
        elif 'list' in args and args_num == 1:
            bucket_name = self.bucket_name
            response = self.BucketUtills.list_bucket(bucket_name, self.db)
            if response != "bucket does not exist.":
                self.set_header("Content-Type", 'application/json') 
                self.write(tornado.escape.json_encode(response))
                self.set_status(200)
            else:
                self.write("bucket does not exist.")
                self.set_status(400)
        else:
            self.write("not found")
            self.set_status(404)
        self.finish()

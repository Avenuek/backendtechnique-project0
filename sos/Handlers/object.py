""" Everything Object """
from mimetypes import MimeTypes
import tornado
from Utils.objectutils import ObjectUtills
from Utils.fileutils import FileUtils

class ObjectHandler(tornado.web.RequestHandler):
    def initialize(self, db):
        args = dict(self.request.arguments).keys()
        path = self.request.path
        path = path.split("/")
        path.remove('')
        self.db = db
        self.bucket_name = path[0]
        self.object_name = path[1]
        self.args_num = len(args)
        self.args = args
        self.ObjectUtils = ObjectUtills()
    def does_bucket_exist(self):
        result = self.db.buckets.find_one({"_id": self.bucket_name})
        return result != None
    def does_object_exist(self):
        result = self.db[self.bucket_name].find_one({"_id": self.object_name})
        return result != None

    def post(self, *args, **kwargs):
        if 'create' in self.args and self.args_num == 1:
            #create upload ticket
            ticket = self.ObjectUtils.creat_upload_ticket(
                    self.bucket_name,self.object_name,self.db)
            if ticket == 'bucket does not exist' or ticket == "duplicate object name or ticket already exist":
                self.write(ticket)
                self.set_status(400)
            else:
                self.write(ticket)
                self.set_status(200)
        elif not self.does_bucket_exist() or not self.does_object_exist():
            self.write("Bucket or Object not found")
            self.set_status(404)
            self.finish()
        elif 'complete' in self.args and self.args_num ==1:
            response, code = self.ObjectUtils.complete_upload(self.bucket_name,self.object_name,self.db)
            self.set_header("Content-Type", 'application/json')
            self.write(tornado.escape.json_encode(response))
            self.set_status(code)
        else:
            self.write("not found")
            self.set_status(404)
        self.finish()

    def put(self, *args, **kwargs):
        content_len_url = int(self.request.headers.get("Content-Length"))
        md5_url = self.request.headers.get("Content-MD5")
        HAS_ALL_UPLOAD_REQUIRMENT = (content_len_url!=None) and (md5_url!=None) and ("partNumber" in self.args and self.args_num == 1)
        if HAS_ALL_UPLOAD_REQUIRMENT:
            #upload
            partNumber = int(self.request.arguments['partNumber'][0])
            body_text = self.request.body
            response, code =self.ObjectUtils.upload_by_part(self.bucket_name, 
                                                    self.object_name,
                                                    self.db,
                                                    md5_url,
                                                    content_len_url,
                                                    partNumber,
                                                    body_text)
            self.set_header("Content-Type", 'application/json')
            self.write(tornado.escape.json_encode(response))
            self.set_status(code)
        elif not self.does_bucket_exist() or not self.does_object_exist():
            self.write("Bucket or Object not found")
            self.set_status(404)
            self.finish()
        elif "metadata" in self.args and "key" in self.args and self.args_num == 2:
            #add or update meta data
            new_metadata= (self.request.arguments["key"][0], self.request.body)
            msg, code = self.ObjectUtils.add_metadata(self.bucket_name, self.object_name, self.db,new_metadata)
            self.write(msg)
            self.set_status(code)
        else:
            self.write("something might went wrong in url, some param might be missing")
            self.write(str(self.args_num))
            self.set_status(404)
        self.finish()
        
    def delete(self, *args, **kwargs):
        if "partNumber" in self.args and self.args_num == 1 :
            # delete part
            partNumber = int(self.request.arguments['partNumber'][0])
            response,code = self.ObjectUtils.delete_part(self.bucket_name, self.object_name,self.db,partNumber)
            if code == 200:
                self.set_status(code)
            else:
                self.set_status(code)
            self.write(response)
        elif not self.does_bucket_exist() or not self.does_object_exist():
            self.write("Bucket or Object not found")
            self.set_status(404)
            self.finish()
        elif "delete" in self.args and self.args_num ==1:
            #delete object
            response = self.ObjectUtils.delete_object(
                self.bucket_name, self.object_name, self.db)
            if response == "bucket does not exist" or response == "cannot delete part becasue the upload already completed":
                self.set_status(400)
            else:
                self.set_status(200)
            self.write(response)
        elif "metadata" in self.args and "key" in self.args and self.args_num == 2:
            #delete metadata
            new_metadata = (self.request.arguments["key"][0], self.request.body)
            msg, code = self.ObjectUtils.delete_metadata(self.bucket_name, self.object_name, self.db, new_metadata)
            self.write(msg)
            self.set_status(code)
        elif "gif" in self.args and self.args_num ==1:
            # delete gif
            msg, code self.ObjectUtils.delete_all_gif(self.bucket_name, self.db)
            self.write(msg)
            self.set_status(code)
        else:
            self.write("not found on DELETE call")
            self.set_status(404)
        self.finish()
        
    def get(self, *args, **kwargs):
        range_header = self.request.headers.get("Range")
        if 'exist' in self.args and self.args_num ==1:
            if self.does_object_exist() and self.does_object_exist():
                self.write("ok")
                self.set_status(200)
            else:
                self.write("not found")
                self.set_status(404)
            self.finish()
            return

        if not self.does_bucket_exist() or not self.does_object_exist():
            self.write("Bucket or Object not found")
            self.set_status(404)
        if self.args_num == 0:
            #download
            if range_header != None:
                content_range = range_header.split("=")[1].split("-")
            else:
                content_range = ['0',''] #get everything
            prepare_download_package = self.ObjectUtils.prepare_download(self.bucket_name, self.object_name, self.db,content_range)
            if len(prepare_download_package) == 2:#cannot download
                error = prepare_download_package
                msg, code = error
                self.write(msg)
                self.set_status(code)
            else: #ready to download
                obj = self.db[self.bucket_name].find_one({"_id":self.object_name})
                etag = obj["eTag"]
                self.set_header('eTage', etag)
                mime = MimeTypes()
                mime_type = mime.guess_type(self.object_name)
                self.set_header('Content-Type', mime_type[0])
                self.set_header('Content-Disposition', 'attachment; filename=' + self.object_name)
                buffer_data = self.ObjectUtils.get_download_chuck(self.bucket_name, prepare_download_package)
                for chuck in buffer_data:
                    for bytes_ in chuck:
                        self.write(bytes_)
                    self.flush()
                self.set_status(200)
        elif "metadata" in self.args and "key" in self.args and self.args_num == 2:
            # Get object metadata by key
            new_metadata = (self.request.arguments["key"][0], self.request.body)
            ret_json, code = self.ObjectUtils.get_metadata( self.bucket_name, self.object_name, self.db, new_metadata)
            self.set_header("Content-Type", 'application/json')
            self.write(tornado.escape.json_encode(ret_json))
            self.set_status(code)
        elif "metadata" in self.args and self.args_num == 1:
            # get object metadata
            new_metadata = (None, None)
            ret_json, code = self.ObjectUtils.get_metadata(self.bucket_name, self.object_name, self.db, new_metadata)
            self.set_header("Content-Type", 'application/json')
            self.write(tornado.escape.json_encode(ret_json))
            self.set_status(code)
        else:
            self.write("not found")
            self.set_status(404)
            self.finish()

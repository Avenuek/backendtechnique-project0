"""
This module is th heart of this system
"""
import logging
import os
import tornado.ioloop as ioloop
import tornado.httpserver as httpserver
import tornado.web   
from pymongo import MongoClient
from Handlers.sandbox import Sandbox
from Handlers.bucket import BucketHandeler
from Handlers.object import ObjectHandler

LOG = logging
LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
DB_NAME = os.getenv("DB_NAME", "localhost")

def conncet_to_db():
    client = MongoClient(DB_NAME, 27017)
    db = client['project0-db']
    return db

if __name__ == "__main__":
    if not os.path.exists("./buckets"):
        os.makedirs("./buckets")
    db = conncet_to_db()
    LOG.info("connected to db...")
    LOG.info("starting sos server at port 8080... ")
    HTTP_SERVER = httpserver.HTTPServer(tornado.web.Application([
        (r"/", Sandbox),
        (r"^/[a-zA-Z0-9-_]+$", 
         BucketHandeler, dict(db=db)),
        (r"^/[a-zA-Z0-9-_]+/(?!\.)[a-zA-Z0-9-_.]+(?<!\.)",
         ObjectHandler, dict(db=db))
    ], debug=True))
    HTTP_SERVER.listen(8080)
    ioloop.IOLoop.current().start()

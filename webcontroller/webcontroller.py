"""
This is the controller
"""
import os
import logging
import requests
import redis
import tornado.ioloop as ioloop
import tornado.httpserver as httpserver
import tornado.web

LOG = logging
LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
DOAMIN = os.getenv("SOS_DOMAIN", "localhost")
PORT = os.getenv("SOS_PORT", 8080)
BASE_URL = f"http://{DOAMIN}:{PORT}"
class RedisResource:
    # Get ENV Variable 'REDIS_QUEUE'
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    QUEUE_NAME = 'queue:thumbnail'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)


class Controller(tornado.web.RequestHandler):

    def get(self):
        self.write("hello from web controller")

    def post(self, *args, **kwargs):
        body = self.request.body
        length = len(body)
        OK = True
        if length > 0 :
            json_packed = tornado.escape.json_decode(self.request.body)
            bucket = json_packed.get("bucket", None)
            file = json_packed.get("file",None)
            msg = json_packed.get("text", "")
            code = 200
            if file == None and bucket == None:
                OK = False
                ret_json = {"status": "bad request", "message": "no required params...."}
                code = 400
            if file == None and bucket != None:
                resp = requests.get(f"{BASE_URL}/{bucket}?exist")
                if resp.status_code == 404:
                    ret_json = {'status': 'FILE NOT FOUND'}
                    code = 404
                else:
                    #todo: write get every file.
                    resp = requests.get(f"{BASE_URL}/{bucket}?list")
                    resp_json = resp.json()
                    objects = resp_json['objects']
                    extensions = set(["mov", "mp4", "m4a", "3gp", "3g2", "mj2"])
                    for o in objects:
                        file = o["name"]
                        extension = file.split(".")[-1]
                        if extension in extensions:
                                RedisResource.conn.rpush(
                                    RedisResource.QUEUE_NAME,
                                    tornado.escape.json_encode({"bucket":bucket, "file":file,"text":msg}))
            elif file != None and bucket != None:
                extension = file.split(".")[-1]
                extensions = set(["mov", "mp4", "m4a", "3gp", "3g2", "mj2"])
                if extension in extensions:
                    resp = requests.get(f"{BASE_URL}/{bucket}/{file}?exist")
                    if resp.status_code == 404:
                        OK = False
                        ret_json = {'status': 'FILE NOT FOUND'}
                        code = 404
                    else:
                        #single file
                        RedisResource.conn.rpush(
                            RedisResource.QUEUE_NAME,
                            tornado.escape.json_encode({"bucket": bucket, "file": file, "text": msg}))
                else:
                    OK = False
                    ret_json = {"status":"bad extension"}
                    code = 400
            else:
                OK = False
                ret_json = {"status": "SHOULD NOT GET HERE",
                            "message": "Webcontrollre screww up some where"}
                code = 500
            if OK:
                ret_json = {"status": "OK"}
                code = 200
            self.set_header("Content-Type", "application/json")
            self.write(tornado.escape.json_encode(ret_json))
            self.set_status(code)
        else:
            ret_json = {"status": "Bad Request","message": "empty body"}
            self.set_header("Content-Type", "application/json")
            self.write(tornado.escape.json_encode(ret_json))
            self.set_status(400)

if __name__ == "__main__":
    LOG.info('Starting controller at port 5000 ...')
    HTTP_SERVER = httpserver.HTTPServer(tornado.web.Application([
        (r"/thumbnail", Controller),
    ], debug=True))
    HTTP_SERVER.listen(5000)
    ioloop.IOLoop.current().start()

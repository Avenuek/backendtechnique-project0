import tornado.web
import requests

class DisplayRoomHandler(tornado.web.RequestHandler):
    def initialize(self):
        path = self.request.path
        path = path.split("/")
        path.remove('')
        self.bucket_name = path[0]

    def get(self):
        resp = requests.get(f"http://localhost:8080/{self.bucket_name}?list")
        objects = (resp.json()['objects'])
        print("\n", objects, "\n")
        html = "./../html/display_room.html"
        # items = ["Item 1", "Item 2", "Item 3"]
        list_example = []
        for obj in objects:
            file = obj['name']
            if file.split(".")[-1] == "gif":
                list_example.append(
                    (file,f"http://localhost:8080/{self.bucket_name}/{file}")
                )
        self.render(html,
                    title=f"{self.bucket_name}",
                    bucketname=self.bucket_name,
                    list_example=list_example
                    )
